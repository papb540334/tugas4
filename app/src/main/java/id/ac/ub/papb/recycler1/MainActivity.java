package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {
    private EditText etNama, etNim;
    private Button bSimpan;
    RecyclerView rv1;
    public static String TAG = "RV1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
        rv1 = findViewById(R.id.rv1);
        ArrayList<Mahasiswa> data = getData();
        refreshRecylerView(data);

        bSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String namaMhs = etNama.getText().toString();
                String nimMhs = etNim.getText().toString();

                Mahasiswa mhs = new Mahasiswa();
                mhs.nim = namaMhs;
                mhs.nama = nimMhs;
                ArrayList<Mahasiswa> newData = data;
                newData.add(mhs);
                refreshRecylerView(newData);
            }
        });

    }

    private void refreshRecylerView(ArrayList<Mahasiswa> data) {
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
    }

    private void bindView() {
        etNama = findViewById(R.id.nama);
        etNim = findViewById(R.id.nim);
        bSimpan = findViewById(R.id.bt1);
    }

    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }
}